<?php

/* START PAGE VARIABLES */

	$SITE_ROOT = './';

/* END PAGE VARIABLES */



/* START EXTERNALS */

	require($SITE_ROOT . 'setup.php');

/* END EXTERNALS */



/* START PAGE RENDER */

	require($DIRECTORY['templates'] . $page_template . '.php');

/* END PAGE RENDER */