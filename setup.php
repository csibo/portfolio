<?php



/************************************************************************************/
/*                                 START INIT                                       */
/************************************************************************************/

	/* START CONSTANTS */
	
		require($SITE_ROOT . 'application/php/constants/directory.php');
		require($DIRECTORY['php constants'] . 'global_constants.php');
		
		date_default_timezone_set('ZULU');
		
	/* END CONSTANTS */
	
	
	
	/* START EXTERNALS */
	
		require($SITE_ROOT . 'settings.php');
	
		require($DIRECTORY['php'] . 'utilities/general.php');
		require($DIRECTORY['php'] . 'utilities/php_utilities.php');
		require($DIRECTORY['php'] . 'utilities/file_utilities.php');
		require($DIRECTORY['php'] . 'utilities/html_utilities.php');
		require($DIRECTORY['php'] . 'utilities/network_utilities.php');
		require($DIRECTORY['php'] . 'utilities/datetime_utilities.php');
		require($DIRECTORY['php'] . 'utilities/email_utilities.php');
	
	/* END EXTERNALS */
	
	

	/* START DIAGNOSTICS */
	
		$Diagnostics = new Diagnostics();
	
	/* END DIAGNOSTICS */
	
	
	
	/* START PAGE VARIABLES */
	
		// Defines the browser's display title for this page.
		$page_title = 'Home';
		
		
		
		// The non-templated content for the page.
		// Directory and extension are automatic; this is only the filename of the desired content in pages/
		$page_template = '1_column';
		$page_body = requestVar('page', 'home');
		$page_mode = requestVar('mode');
		$page_action = requestVar('action');
		
		
		
		// Get any specified page redirect location.
		$redirect = urldecode(requestVar('redirect', ''));
		
		
		
		/* START STYLES */
		
			/* 	
				List of the CSS files to import.
				For local files: Directory and extension are automatic; this is only the filenames of the desired content in css/
				For remote files: This is the entire href attribute contents.
			*/
			
			$styles[] = 'http://fonts.googleapis.com/css?family=Orbitron:400,500';
			$styles[] = 'http://fonts.googleapis.com/css?family=Electrolize';
			$styles[] = 'http://fonts.googleapis.com/css?family=Montserrat';
			$styles[] = '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'; 
			$styles[] = '//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css';
			//$styles[] = 'jquery_ui_vader/jquery-ui';
			$styles[] = '//cdn.jsdelivr.net/select2/4.0.0/css/select2.min.css';
			$styles[] = 'animate';
			$styles[] = 'global';
			$styles[] = 'portfolio';
			$styles[] = 'jssor';
			$styles[] = 'select2';
			$styles[] = 'preloader';

		
		/* END STYLES */
		
		
		
		/* START SCRIPTS */
		
			/*
				List of the JavaScript files to import.
				For local files: Directory and extension are automatic; this is only the filenames of the desired content in js/
				For remote files: This is the entire src attribute contents.
			*/
			
			$scripts[] = '//code.jquery.com/jquery-1.10.2.min.js';
			$scripts[] = '//code.jquery.com/ui/1.11.3/jquery-ui.min.js';
			$scripts[] = 'slider-master/js/jssor';
			$scripts[] = 'slider-master/js/jssor.slider';
			$scripts[] = 'slider-master/js/jssor.player.ytiframe.js';
			//$scripts[] = 'slider-master/js/jssor.slider.mini';
			$scripts[] = 'jquery_onresize.min';
			$scripts[] = 'jquery.nicescroll/jquery.nicescroll.min';
			$scripts[] = '//cdn.jsdelivr.net/select2/4.0.0/js/select2.min.js';
			$scripts[] = '//cdn.jsdelivr.net/velocity/1.2.2/velocity.min.js';
			$scripts[] = '//cdn.jsdelivr.net/velocity/1.2.2/velocity.ui.min.js';
			$scripts[] = 'global';
			$scripts[] = 'classes/Page';
			$scripts[] = 'jquery.sticky-kit.min';
		
		/* END SCRIPTS */
		
		
		
		// Tracks the position of the user as they navigate through pages.
		$Breadcrumbs = new Breadcrumbs();
		$Breadcrumbs->Add('Home', '/');
	
	
	
		// Connect to database.
		$DB = new DatabaseHandler();
		$DB->Connect('portfolio');
	
	/* END PAGE VARIABLES */

/************************************************************************************/
/*                                  END INIT                                        */
/************************************************************************************/