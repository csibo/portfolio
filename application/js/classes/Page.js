﻿
var CurrentPage = null;
var tempParams = null;



function Page(obj) 
{
	this.target = obj;

	
	
	Page.prototype.update = function(data)
	{
		this.target.html(data);
	
		this.target.velocity('fadeIn', 400);
		
		
		setTimeout(function()
		{
			$('.select2').not('.hasSelect2').addClass('hasSelect2').select2();
			
			$('.select2-clearable').select2(
			{
				allowClear: true
			});
		}, 100);
		
		
	
		$("#tabs").tabs();
		
		setTimeout(function()
		{
			$("body").getNiceScroll().resize();
		}, 1000);
		
		
	
		$(".ajax").click(function(event)
		{
			$(CurrentPage).changePage({'page': $(this).data('page'),'mode': $(this).data('mode'), 'action': $(this).data('action')}, {'state': {}, 'title': 'TEST', 'location': $(this).attr('href')});
			
			event.preventDefault();
		});
		
		$(".ajax-widget-select").change(function(event)
		{
			$(this).data($(this).attr('name'), $(this).val());
			
			if($(this).hasClass('select2'))
			{
				$(this).select2('destroy');
			}
			
			changePage($(this).data(), {'state': {}, 'title': '', 'location': ''}, 0);
			
			//event.preventDefault();
		});
	};
	
	
	
	Page.prototype.refresh = function(params) 
	{
		$.post
		(
			'/application/php/ajax/get_page.php',
			
			$.extend
			(
				{},
				$_GET,
				$_POST,
				params
			),
			
			$.proxy
			(
				this.update,
				this
			)
		);
	};
	
	
	
	this.refresh();
};



$.fn.page = function() 
{
	return new Page($(this));
};



$.fn.changePage = function(params, newHistory)
{
	tempParams = params;
	
	history.pushState($.extend({}, $_GET, params), newHistory.title, newHistory.location);
	
	CurrentPage.target.velocity('fadeOut', 400, doRefresh);
}



changePage = function(params, newHistory)
{
	tempParams = params;
	
	history.pushState($.extend({}, $_GET, params), newHistory.title, newHistory.location);
	
	CurrentPage.target.velocity('fadeOut', 400, doRefresh);
}



doRefresh = function()
{
	CurrentPage.refresh(tempParams);
	
	tempParams = null;
}



$(function()
{
	CurrentPage = $('.page-data-target').page();
	
	history.pushState($_GET, document.title, document.location.href);
});



window.addEventListener('popstate', function(event) 
{
	$.extend($_GET, event.state);
	
	CurrentPage.refresh();
});
