
var mouseX, mouseY;
var timeout_var;
var jssor_slider1 = null;


var _SlideshowTransitions = 
[
	//Fade
	//{ $Duration: 1200, $Opacity: 2 },
	
	//Fade Clip in H
	{$Duration:1200,$Delay:20,$Clip:3,$Assembly:260,$Easing:{$Clip:$JssorEasing$.$EaseInCubic,$Opacity:$JssorEasing$.$EaseLinear},$Opacity:2}
];

var sliderOptions = 
{
	$FillMode: 1,
	$AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
	$AutoPlayInterval: 5000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
	$PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

	$ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
	$SlideDuration: 800,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
	$MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
	//$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
	//$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
	$SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
	$DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
	$ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
	$UISearchMode: 0,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
	$PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
	$DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

	$SlideshowOptions: 
	{                                //[Optional] Options to specify and enable slideshow or not
		$Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
		$Transitions: _SlideshowTransitions,            //[Required] An array of slideshow transitions to play slideshow
		$TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
		$ShowLink: false                                    //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
	},
	
	/*
	$ArrowNavigatorOptions: 
	{                       //[Optional] Options to specify and enable arrow navigator or not
		$Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
		$ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
		$AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
		$Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
	},

	$ThumbnailNavigatorOptions: 
	{
		$Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
		$ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

		$ActionMode: 1,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
		$AutoCenter: 0,                                 //[Optional] Auto center thumbnail items in the thumbnail navigator container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 3
		$Lanes: 1,                                      //[Optional] Specify lanes to arrange thumbnails, default value is 1
		$SpacingX: 3,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
		$SpacingY: 3,                                   //[Optional] Vertical space between each thumbnail in pixel, default value is 0
		$DisplayPieces: 9,                              //[Optional] Number of pieces to display, default value is 1
		$ParkingPosition: 260,                          //[Optional] The offset position to park thumbnail
		$Orientation: 1,                                //[Optional] Orientation to arrange thumbnails, 1 horizental, 2 vertical, default value is 1
		$DisableDrag: false                            //[Optional] Disable drag or not, default value is false
	}
	*/
	
	$ThumbnailNavigatorOptions: 
	{
		$Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
		$ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

		$Loop: 1,                                       //[Optional] Enable loop(circular) of carousel or not, 0: stop, 1: loop, 2 rewind, default value is 1
		$SpacingX: 3,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
		$SpacingY: 3,                                   //[Optional] Vertical space between each thumbnail in pixel, default value is 0
		$DisplayPieces: 6,                              //[Optional] Number of pieces to display, default value is 1
		$ParkingPosition: 204,                          //[Optional] The offset position to park thumbnail,

		$ArrowNavigatorOptions: 
		{
			$Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
			$ChanceToShow: 0,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
			$AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
			$Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
		}
	}
};



var uid = function(i) 
{
    return function() 
	{ 
        return 'uid-' + (++i); 
    };
}(0);

















function applySlider()
{
	jssor_slider1 = new $JssorSlider$("slider1_container", sliderOptions);
		
	function ScaleSlider1()
	{
		var bodyWidth = $("#slider1_container").parent().width();

		if (bodyWidth > 0)
		{
			jssor_slider1.$ScaleWidth(Math.min(bodyWidth, 1920));
		}
		else
		{
			window.setTimeout(ScaleSlider1, 30);
		}
	}

	ScaleSlider1();

	$(window).bind("load", ScaleSlider1);
	$(window).bind("resize", ScaleSlider1);
	$(window).bind("orientationchange", ScaleSlider1);
	$(".centerCol").bind("resize", ScaleSlider1);

	jQuery.resize.delay = 250;
}






// Fires when the page is ready for rendering.
jQuery(document).ready(function($) 
{
	$(document).mousemove(function(e)
	{
		mouseX = e.pageX;
		mouseY = e.pageY;
	});
	
	var icons = 
	{
		header: "ui-icon-circle-arrow-e",
		activeHeader: "ui-icon-circle-arrow-s"
	};
	
	$("#accordion").accordion(
	{
		collapsible: true,
		//icons: icons,
		icons:false,
		heightStyle: "content",
		active: false,
	});
	
	$("#accordion2").accordion(
	{
		collapsible: true,
		//icons: icons,
		heightStyle: "content",
		active: false,
		icons:false,
	});
	
	
	// Show the page.
	$('#preloader').velocity("fadeOut", { duration: 1000 });
	
    $('.select2').select2();
	
    $('.select2-clearable').select2(
	{
		allowClear: true
	});
	
	
	$("body").niceScroll(
	{
		autohidemode:true,
		railpadding:
		{
			top:0,
			right:0,
			left:0,
			bottom:0
		},
		railoffset:
		{
			top:0,
			left:10
		},
		cursorwidth: 15,
		bouncescroll:true,
		hidecursordelay:1000,
		mousescrollstep:30,
		scrollspeed:100,
		cursorborder:"1px solid rgb(100,100,100)"
	});
});

var vis = 
(
	function()
	{
		var stateKey, eventKey, keys = 
		{
			hidden: "visibilitychange",
			webkitHidden: "webkitvisibilitychange",
			mozHidden: "mozvisibilitychange",
			msHidden: "msvisibilitychange"
		};
		
		for (stateKey in keys) 
		{
			if (stateKey in document) 
			{
				eventKey = keys[stateKey];
				break;
			}
		}
		
		return function(c) 
		{
			if (c)
			{
				document.addEventListener(eventKey, c);
			}
			
			return !document[stateKey];
		}
	}
)();