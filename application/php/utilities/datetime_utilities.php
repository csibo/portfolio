<?php

// Turn a given date into a relative time from now.
// ex. "3 minutes"
function relativeTime($date)
{
	$seconds_in = array
	(
		0 => 60,
		1 => 60,
		2 => 24,
		3 => 30,
		4 => 12,
	);
	
	$names = array
	(
		-1 => 'seconds',
		0 => 'minutes',
		1 => 'hours',
		2 => 'days',
		3 => 'months',
		4 => 'years',
	);

	$diff = time() - $date;
	
	if($diff < 0)
	{
		return '0 seconds';
	}
	
	$i=0;
	
	while($diff / $seconds_in[$i] > 1 && $seconds_in[$i] > 0)
	{
		$diff = $diff / $seconds_in[$i];
		
		$i++;
	}
	
	return round($diff, 0) . ' ' . $names[$i-1];
}