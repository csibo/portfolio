<?php

function getPage($url, $custom_headers = array())
{
	$agent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0";

	$headers = $custom_headers;
	$headers[] = "Accept: */*";
	$headers[] = "Connection: Keep-Alive";

	// Begin curl.
	$ch = curl_init();

	// Basic curl options for all requests.
	curl_setopt($ch, CURLOPT_HTTPHEADER,  $headers);
	curl_setopt($ch, CURLOPT_HEADER,  0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
	curl_setopt($ch, CURLOPT_USERAGENT, $agent);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	
	curl_setopt($ch, CURLOPT_URL, $url);
	
	// Run request.
	$result = curl_exec($ch);
	
	// Close request.
    curl_close($ch);
	
	return $result;
}



// Gets current absolute page URL.
function curPageURL()
{
    $pageURL = 'http';
	
	// Add the 's' at the end of 'https', if required.
    if ($_SERVER["HTTPS"] == "on")
	{
		$pageURL .= "s";
	}
	
	$pageURL .= "://";
	
	// Handle non-default ports.
    if ($_SERVER["SERVER_PORT"] != "80")
	{
		$pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    }
	else
	{
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
	
    return $pageURL;
}