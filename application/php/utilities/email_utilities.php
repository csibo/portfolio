<?php

function send_formatted_email($message_array)
{
    if($message_array['to_email'] != null)
    {
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: no-reply@thalos.org'. "\r\n";
        $headers .= 'Reply-To: no-reply@thalos.org'. "\r\n";
        $headers .= 'X-Mailer: php';

        
        $template=
'
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>'.$message_array['subject'].'</title>
    <style type="text/css">
    @font-face {
      font-family: "Electrolize";
      font-style: normal;
      font-weight: 400;
      src: local("Electrolize"), local("Electrolize-Regular"), url(https://themes.googleusercontent.com/static/fonts/electrolize/v2/DDy9sgU2U7S4xAwH5thnJ4bN6UDyHWBl620a-IRfuBk.woff) format("woff");
    }
    body{
        width:100% !important;
        font-family: Arial, Helvetica, sans-serif !important;
        margin:0 !important;
        padding:0 !important;
        -webkit-text-size-adjust:none;
        color:#FFFFFF;
        background-image:url("http://thalos.org/images/pinhole_small.jpg");
    }
    img{
        border:none;
        height:auto;
        line-height:100%;
        margin:0;
        outline:none;
        padding:0;
        text-decoration:none;
    }
    a
    {
        text-shadow:0px 0px 5px #00EEFF;
    }
    table
    {
        background-image:url("http://thalos.org/images/pinhole_small.jpg");
    }
    h1, h2, h3, h4
    {
        text-shadow:0px 0px 5px #44FFFF;
    }
    .panel
    {
            position:relative;
            background-color: #282828;
            background-color:#303030;
            width:900px;
            margin:0;
            padding:30px;
            padding-bottom:50px;
            margin-bottom:30px;
            border-radius:0px 0px 5px 100px;
            font-size:12px;

            top:-15px;

            -webkit-box-shadow: -5px 5px 5px 0px #00C0FF;
               -moz-box-shadow: -5px 5px 5px 0px #00C0FF;
                    box-shadow: -5px 5px 5px 0px #00C0FF;
    }
    .banner
    {
        -webkit-box-shadow: -5px 0px 5px 0px #00C0FF;
           -moz-box-shadow: -5px 0px 5px 0px #00C0FF;
                box-shadow: -5px 0px 5px 0px #00C0FF;
    }
    a { color: white; }
    </style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust: none;margin: 0 !important;padding: 0 !important;background-color: #0c1015;width: 100% !important;font-family: Arial, Helvetica, sans-serif !important;">
<center>
    <table border="0" cellpadding="0" cellspacing="0" rowspacing="0" width="100%" style="background-color: #0c1015; margin: 0 !important; padding: 0 !important;">
        <tbody>
            <tr align="center">
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" rowspacing="0" width="650" align="center" style="margin: 0 !important; padding: 0 !important;">
                        <tbody>
                            <tr>
                                <td align="left" class="panel">
                                    '.$message_array['body'].'
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</center>
</body>
</html>
';
        
        mail($message_array['to_email'], $message_array['subject'], $template, $headers);
    }
}