<?php

spl_autoload_register
(
	function($className) 
	{
		global $SITE_ROOT;
		
		$dir = recursive_file_search($className . '.php', $SITE_ROOT . 'application/php/classes/');
		
		if($dir != false) 
		{
			require_once($dir . $className . '.php');
			
			return true;
		}
		
		return false;
	}
);



function class_can_be_autoloaded($className) 
{
	global $SITE_ROOT;
	
    return class_exists(recursive_file_search($className . '.php', $SITE_ROOT . 'application/php/classes/') . $className . '.php');
}



// Gets page parameters passed by POST or GET.
function requestVar($var_name, $default_val = null)
{
	// Check for POST params first.
	if(isset($_POST[$var_name]) && $_POST[$var_name] != '')
	{
		return $_POST[$var_name];
	}
	// Check for GET params second.
	else if(isset($_GET[$var_name]) && $_GET[$var_name] != '')
	{
		return $_GET[$var_name];
	}
	
	// Report default value.
	return $default_val;
}