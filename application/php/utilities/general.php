<?php

function createWidget($widget_id)
{
	global $PortalDB, $User, $page_body, $page_mode, $page_action;
	
	$widget = $PortalDB->Fetch('SELECT * FROM widgets WHERE widget_id = ?', array($widget_id));
	$widget['content_parameters'] = json_decode($widget['content_parameters']);
	
	$args = array('trash' => 'junk');
	if(count($widget['content_parameters']) > 0)
	{
		foreach($widget['content_parameters'] as $key => $value)
		{
			$args[$key] = eval('return ' . $value . ';');
		}
	}
	
	$widget['content_parameters'] = $args;
	
	$permitted = eval('return ' . $widget['permission_logic'] . ';');
	
	if($permitted == 0)
	{
		$widget['content_url'] = 'denied';
	}
	
	return $widget;
}



function make_paginated_links($base_link, $number_of_pages, $current_page, $pretty_url = false)
{
    $links = '';
    
    $overflow_delimiter = ' ... ';
    
    if($number_of_pages > 1)
    {
		if($current_page > 1)
		{
			$links .= '<a href="' . $base_link;
			
			if($pretty_url)
			{
				$links .= $current_page - 1;
			}
			else
			{
				$links .= '&page=' . ($current_page - 1);
			}
			
			$links .= '" class="paginated_link"><i class="fa fa-arrow-left"></i></a>&nbsp;';
		}
		
		
		
		
        $links .= '<a href="' . $base_link;
		
		if($pretty_url)
		{
			$links .= '1';
		}
		else
		{
			$links .= '&page=1';
		}
		
		$links .= '" class="paginated_link';
		
        if($current_page == 1)
        {
            $links .= ' selected_paginated_link';
        }
		
        $links .= '">1</a>  ';
    }

    $start_page = 2;
    if($current_page - 4 > 1)
    {
        $start_page = $current_page - 4;
    }

    $end_page = $number_of_pages;
    if($current_page + 4 < $number_of_pages)
    {
        $end_page = $current_page + 4;
    }

    if($start_page > 2)
    {
        $links .= $overflow_delimiter;
    }

    for($i=$start_page; $i<$end_page; $i++)
    {
        $links .= '<a href="' . $base_link;
		
		if($pretty_url)
		{
			$links .= $i;
		}
		else
		{
			$links .= '&page=' . $i;
		}
		
		$links .= '" class="paginated_link';
		
        if($current_page == $i)
        {
            $links .= ' selected_paginated_link';
        }
		
        $links .= '">'.$i.'</a>  ';
    }

    if($end_page < $number_of_pages)
    {
        $links .= $overflow_delimiter;
    }

    $links .= '<a href="'.$base_link;
		
	if($pretty_url)
	{
		$links .= $number_of_pages;
	}
	else
	{
		$links .= '&page=' . $number_of_pages;
	}
	
	$links .= '" class="paginated_link';
	
    if($current_page == $number_of_pages)
    {
        $links .= ' selected_paginated_link';
    }
	
    $links .= '">'.$number_of_pages.'</a>';
	
	
	
	
	if($current_page < $end_page)
	{
		$links .= '&nbsp;<a href="' . $base_link;
			
		if($pretty_url)
		{
			$links .= $current_page + 1;
		}
		else
		{
			$links .= '&page=' . ($current_page + 1);
		}
		
		$links .= '" class="paginated_link"><i class="fa fa-arrow-right"></i></a>';
	}
	
	
    
    return $links;
}

function widget_makePaginatedLinks($base_link, $number_of_pages, $current_page, $pretty_url = false)
{
	/* <a onClick="history.pushState({},'AMS','/hub/ams/list/<?=($page + 1)?>');getWidget(this).refresh({'pageNum': '<?=($page + 1)?>'});">Next</a> */
	
    $links = '';
    
    $overflow_delimiter = ' ... ';
    
    if($number_of_pages > 1)
    {
		if($current_page > 1)
		{
			$links .= '<a onClick="history.pushState($.extend({\'pageNum\': \'' . ($current_page - 1) . '\'}, $_GET), \'{TITLE}\', \'' . $base_link;
			
			if($pretty_url)
			{
				$links .= $current_page - 1;
			}
			else
			{
				$links .= '&page=' . ($current_page - 1);
			}
			
			$links .= '\');$(this).getWidget().refresh({\'pageNum\': \'' . ($current_page - 1) . '\'});" class="paginated_link"><i class="fa fa-arrow-left"></i></a>&nbsp;';
		}
		
		
		
		
        $links .= '<a onClick="history.pushState($.extend({\'pageNum\': \'' . (1) . '\'}, $_GET), \'{TITLE}\', \'' . $base_link;
		
		if($pretty_url)
		{
			$links .= '1';
		}
		else
		{
			$links .= '&page=1';
		}
		
		$links .= '\');$(this).getWidget().refresh({\'pageNum\': \'1\'});" class="paginated_link';
		
        if($current_page == 1)
        {
            $links .= ' selected_paginated_link';
        }
		
        $links .= '">1</a>  ';
    }

    $start_page = 2;
    if($current_page - 4 > 1)
    {
        $start_page = $current_page - 4;
    }

    $end_page = $number_of_pages;
    if($current_page + 4 < $number_of_pages)
    {
        $end_page = $current_page + 4;
    }

    if($start_page > 2)
    {
        $links .= $overflow_delimiter;
    }

    for($i=$start_page; $i<$end_page; $i++)
    {
        $links .= '<a onClick="history.pushState($.extend({\'pageNum\': \'' . ($i) . '\'}, $_GET), \'{TITLE}\', \'' . $base_link;
		
		if($pretty_url)
		{
			$links .= $i;
		}
		else
		{
			$links .= '&page=' . $i;
		}
		
		$links .= '\');$(this).getWidget().refresh({\'pageNum\': \'' . $i . '\'});" class="paginated_link';
		
        if($current_page == $i)
        {
            $links .= ' selected_paginated_link';
        }
		
        $links .= '">'.$i.'</a>  ';
    }

    if($end_page < $number_of_pages)
    {
        $links .= $overflow_delimiter;
    }

    $links .= '<a onClick="history.pushState($.extend({\'pageNum\': \'' . ($number_of_pages) . '\'}, $_GET), \'{TITLE}\', \''.$base_link;
		
	if($pretty_url)
	{
		$links .= $number_of_pages;
	}
	else
	{
		$links .= '&page=' . $number_of_pages;
	}
	
	$links .= '\');$(this).getWidget().refresh({\'pageNum\': \'' . $number_of_pages . '\'});" class="paginated_link';
	
    if($current_page == $number_of_pages)
    {
        $links .= ' selected_paginated_link';
    }
	
    $links .= '">'.$number_of_pages.'</a>';
	
	
	
	
	if($current_page < $end_page)
	{
		$links .= '&nbsp;<a onClick="history.pushState($.extend({\'pageNum\': \'' . ($current_page + 1) . '\'}, $_GET), \'{TITLE}\', \'' . $base_link;
			
		if($pretty_url)
		{
			$links .= $current_page + 1;
		}
		else
		{
			$links .= '&page=' . ($current_page + 1);
		}
		
		$links .= '\');$(this).getWidget().refresh({\'pageNum\': \'' . ($current_page + 1) . '\'});" class="paginated_link"><i class="fa fa-arrow-right"></i></a>';
	}
	
	
    
    return $links;
}

function paginate_query($query, $requested_page, $items_per_page, $base_link)
{
    $return = array();
    
    $all_results = mysql_query($query);
    $return['all_results'] = $all_results;
    
    $all_results = mysql_num_rows($all_results);
    
    if(is_numeric($requested_page)
        && is_numeric($items_per_page))
    {
         $return['num_pages'] = ceil($all_results/$items_per_page); 

        if ($requested_page < 1) 
        { 
            $requested_page = 1; 
        } 
        elseif ($requested_page > $return['num_pages']) 
        { 
            $requested_page = $return['num_pages']; 
        }

        $query .= ' LIMIT ' .($requested_page - 1) * $items_per_page .',' .$items_per_page;
        
        $return['links'] = make_paginated_links($base_link, $return['num_pages'], $requested_page);
    }
    
    $paginated_result = mysql_query($query);
    $return['paginated_results'] = $paginated_result;
    
    return $return;
}



function printPortfolioEntries($entries)
{
	global $DIRECTORY, $DB;
	
	$output = '<div style="display:block;width:95%;margin: 0 auto;">';

	foreach($entries as $index => $entry)
	{
		$categories = @$DB->FetchAllAndFlatten(
			'SELECT 
				portfolio_categories.title
			FROM 
				portfolio_categories, 
				portfolio_category_entries 
			WHERE 
				portfolio_categories.id = portfolio_category_entries.category_id 
				AND portfolio_category_entries.portfolio_id = ' . $entry['id']);
		
		$output .= '<div class="ajax box-wrapper" id="box-wrapper-<' . $index . '" href="/portfolio/page/' . $entry['id'] . '" data-page="portfolio" data-mode="page" data-action="' . $entry['id'] . '">

			<div class="' . (($index % 2 == 0)?'':'right-') . 'box">

				<div class="inner inner0">
				
					<div class="content" id="entry-content-' . $index . '-0">
					
						&nbsp;
						
					</div>
					
				</div>

				<div class="inner inner1">
				
					<div class="content" id="entry-content-' . $index . '-1">
					
						&nbsp;
						
					</div>
					
				</div>

				<div class="inner inner2">
				
					<div class="content" id="entry-content-' . $index . '-2">
					
						&nbsp;
						
					</div>
					
				</div>

				<div class="inner inner3">
				
					<div class="content" style="text-align:center;" id="entry-content-' . $index . '-3">
						
						' . $entry['title'] . '
						
					</div>
					
				</div>

				<div class="inner inner4">
				
					<div class="content" id="entry-content-' . $index . '-4">
					
						&nbsp;
						
					</div>
					
				</div>

				<div class="inner inner5">
				
					<div class="content" id="entry-content-' . $index . '-5">
					
						&nbsp;
						
					</div>
					
				</div>

				<div class="inner inner6">
				
					<div class="content" id="entry-content-' . $index . '-6">
					
						&nbsp;
						
					</div>
					
				</div>
				
			</div>
						
			<div class="' . (($index % 2 == 0)?'left-':'right-') . 'summary summary" id="summary-' . $index . '">

				' . $entry['summary'] . '
			
			</div>
			
			<div class="categories-bar">
				' . implode(', ', $categories) . '
			</div>
			
			';
			
		if($entry['featured'])
		{
			$output .= 
			'<div class="featured-bar vertical-text-reversed2">
				FEATURED
			</div>';
		}
			
		$output .= '
			
			<style>
			
				#entry-content-' . $index . '-0:after,
				#entry-content-' . $index . '-1:after,
				#entry-content-' . $index . '-2:after,
				#entry-content-' . $index . '-3:after,
				#entry-content-' . $index . '-4:after,
				#entry-content-' . $index . '-5:after,
				#entry-content-' . $index . '-6:after,
				#summary-' . $index . ':after
				{
					background-image:url("' . $DIRECTORY['img'] . 'portfolio/' . $entry['thumbnail'] . '");
				}
				
			</style>
		
		</div>';
	}

	$output .= '</div>

	<script>

		$(".box-wrapper").velocity("transition.fadeIn", 
		{
			duration: 1000,
			stagger: 250,
			delay: 0
		});
		
		$(".box-wrapper").hover
		(
			function()
			{
				$(this).find(".summary").mouseover();
			},
				
			function()
			{
				$(this).find(".summary").mouseout();
			}
		);
		
		setTimeout(function()
			{
				$(".featured-bar").each(function()
				{
					$(this).width($(this).parent().height() - 3);
					$(this).css("right", -($(this).width() - 10) + "px");
					$(this).css("top", 6 - (Math.floor(113 / $(this).width()) * 2) + "px");
				});
				
				$(".featured-bar").velocity("transition.fadeIn", 
				{
					duration: 1000,
					stagger: 250,
					delay: 0
				});
			},
			1250);
		
		
	</script>';

	echo $output;// - $(this).parent().css(\'border-top-width\') - $(this).parent().css(\'border-bottom-width\')
}






function printPortfolioEntries2($entries)
{
	global $DIRECTORY, $DB;
	
	$output = 
	'<div style="display:block;">';

	foreach($entries as $index => $entry)
	{
		$categories = @$DB->FetchAllAndFlatten
		(
			'SELECT 
				portfolio_categories.title
			FROM 
				portfolio_categories, 
				portfolio_category_entries 
			WHERE 
					portfolio_categories.id = portfolio_category_entries.category_id 
				AND portfolio_category_entries.portfolio_id = ' . $entry['id']
		);
		
		$output .= 
		'<div class="ajax portfolio-entry-wrapper" id="portfolio-entry-wrapper-' . $index . '" href="/portfolio/page/' . $entry['id'] . '" data-page="portfolio" data-mode="page" data-action="' . $entry['id'] . '">

			<div class="portfolio-entry">

				<div class="inner">
				
					<div class="content" style="text-align:center;" id="entry-content-' . $index . '-3">
						
						' . $entry['title'] . '
						
					</div>
					
				</div>
				
			</div>
			
			<div class="summary" id="summary-' . $index . '">

				' . $entry['summary'] . '
			
			</div>
			
			<div class="categories-bar">
			
				' . implode(', ', $categories) . '
				
			</div>';
		
		
		
		if($entry['featured'])
		{
			$output .= 
			'<div class="featured-bar vertical-text-reversed2">
			
				FEATURED
				
			</div>';
		}
		
		
		
		$output .= 
			'<style>
				#portfolio-entry-wrapper-' . $index . '
				{
					background-image:url("' . $DIRECTORY['img'] . 'portfolio/' . $entry['thumbnail'] . '");
				}
				
			</style>
		
		</div>';
	}

	
	
	$output .= 
	'</div>

	<script>

		$(".portfolio-entry-wrapper").velocity("transition.fadeIn", 
		{
			duration: 1000,
			stagger: 250,
			delay: 0
		});
		
		setTimeout(function()
			{
				$(".featured-bar").each(function()
				{
					$(this).width($(this).parent().height());
					$(this).css("right", -($(this).width() - 10) + "px");
					
					switch(Math.ceil($(this).width() / 168))
					{
						case 1:
						{
							$(this).css("top", 7 + "px");
							break;
						}
						
						case 2:
						{
							$(this).css("top", 10 + "px");
							break;
						}
					}
				});
				
				$(".featured-bar").velocity("transition.fadeIn", 
				{
					duration: 1000,
					stagger: 250,
					delay: 0
				});
			},
			1250);
		
		
	</script>';

	
	
	echo $output;
}
