<?php

function redirectTo($url)
{
	if(!headers_sent()) 
	{
		header('Location: ' . $url);
	}
	else
	{
		echo
		'<script>
			window.location = "' . $url . '";
		</script>';
	}
}