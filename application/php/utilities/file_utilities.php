<?php

/*
 * @Search recursively for a file in a given directory.
 *
 * @param string $filename The file to find.
 *
 * @param string $directory The directory to search.
 *
 * @return bool
 *
 */
function recursive_file_exists($filename, $directory)
{
    foreach
	(
		$iterator = new RecursiveIteratorIterator
		(
			new RecursiveDirectoryIterator
			(
				$directory, 
				RecursiveDirectoryIterator::SKIP_DOTS
			),
			
			RecursiveIteratorIterator::SELF_FIRST
			
		) as $value
	) 
	{

		if($value->getFilename() == $filename) 
		{
			return true;
		}   
	}
	
	return false;
}



/*
 * @Search recursively for a file in a given directory.
 *
 * @param string $filename The file to find.
 *
 * @param string $directory The directory to search.
 *
 * @return string
 *
 */
function recursive_file_search($filename, $directory)
{
	foreach
	(
		$iterator = new RecursiveIteratorIterator
		(
			new RecursiveDirectoryIterator
			(
				$directory, 
				RecursiveDirectoryIterator::SKIP_DOTS
			),
			
			RecursiveIteratorIterator::SELF_FIRST
			
		) as $value
	) 
	{

		if($value->getFilename() == $filename) 
		{
			return $value->getPath() . '/';
		}   
	}
	
	return false;
}



function readableFileSize($size)
{
    $unit=array('B','KiB','MiB','GiB','TiB','PiB');
	
    return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
}