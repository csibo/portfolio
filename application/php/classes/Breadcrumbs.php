<?php

class Breadcrumbs
{
	protected $breadcrumbs;
    
	// Default constructor.
    public function __construct()
    {
		$this->breadcrumbs = array();
    }
	
	public function Add($title, $link)
	{
		$this->breadcrumbs[] = array('title' => $title, 'link' => $link);
	}
	
	public function Render($delimiter)
	{
		$breadcrumbs_display = '';

		// Loop through our breadcrumbs.
		foreach($this->breadcrumbs as $index => $breadcrumb)
		{
			// Place delimiter between subsequent crumbs.
			if($index > 0)
			{
				$breadcrumbs_display .= $delimiter;
			}
			
			// Only include the link if it isn't the current page.
			if($index < count($this->breadcrumbs) - 1)
			{
				preg_match('|/?(.+)?/?(.+)?/?(.+)?/?|', $breadcrumb['link'], $matches);
				
				$breadcrumbs_display .= '<a class="ajax" href="'. $breadcrumb['link'] .'" data-page="' . $matches[1] . '" data-mode="' . $matches[2] . '" data-action="' . $matches[3] . '">';
			}
			
			// Display the crumb.
			$breadcrumbs_display .= $breadcrumb['title'];
			
			// Only include the link if it isn't the current page.
			if($index < count($this->breadcrumbs) - 1)
			{
				$breadcrumbs_display .= '</a>';
			}
		}
		
		$breadcrumbs_display .= "\n";
		
		return $breadcrumbs_display;
	}
}