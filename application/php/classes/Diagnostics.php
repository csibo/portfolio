<?php

class Diagnostics
{
	public $databases;
	
	protected $phpStartTime;
    
	// Default constructor.
    public function __construct()
    {
		$this->phpStartTime = $_SERVER['REQUEST_TIME_FLOAT'];
		
		$this->databases = array();
    }
	
	public function ExecutionTime()
	{
		return microtime(true) - $this->phpStartTime;
	}
	
	public function DatabaseQueries()
	{
		$count = 0;
		
		if(count($this->databases) > 0)
		{
			foreach($this->databases as $db)
			{
				$count += $db->queryCount;
			}
		}
		
		return $count;
	}
	
	public function DatabaseCount()
	{
		return count($this->databases);
	}
	
	public function PeakMemory($realOnly = false)
	{
		return memory_get_peak_usage($realOnly);
	}
	
	public function RenderReport()
	{
		echo
		'<table style="width:100%;background-color:rgba(0,0,0,0.8);margin-top:10px;border-radius:5px;padding:5px;float:left;border:1px solid rgb(60,60,60);text-align:center;font-size:80%;">
			<tr>
				<td>
					' . (number_format($this->ExecutionTime(), 3)) . 's
				</td>
				
				<td>
					' . $this->DatabaseQueries() . ' db queries
				</td>
				
				<td>
					' . $this->DatabaseCount() . ' db cnctions
				</td>
				
				<td>
					' . readableFileSize($this->PeakMemory()) . '
				</td>
			</tr>
		</table>';
	}
}