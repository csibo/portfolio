<?php

$DIRECTORY['application'] 		= $SITE_ROOT . 'application/';

	$DIRECTORY['php'] 			= $DIRECTORY['application'] . 'php/';
	$DIRECTORY['php classes'] 	= $DIRECTORY['application'] . 'php/classes/';
	$DIRECTORY['php constants'] = $DIRECTORY['application'] . 'php/constants/';
	$DIRECTORY['js'] 			= $DIRECTORY['application'] . 'js/';
	
	

$DIRECTORY['presentation'] 		= $SITE_ROOT . 'presentation/';

	$DIRECTORY['css'] 			= $DIRECTORY['presentation'] . 'css/';
	$DIRECTORY['fonts'] 		= $DIRECTORY['presentation'] . 'fonts/';
	$DIRECTORY['img'] 			= $DIRECTORY['presentation'] . 'img/';
	$DIRECTORY['templates'] 	= $DIRECTORY['presentation'] . 'templates/';
	$DIRECTORY['pages'] 		= $DIRECTORY['presentation'] . 'pages/';



$PRETTY_DIRECTORY['css'] = '/presentation/css/';
$PRETTY_DIRECTORY['img'] = '/presentation/img/';

$PRETTY_DIRECTORY['js'] = '/application/js/';
$PRETTY_DIRECTORY['php'] = '/application/php/';