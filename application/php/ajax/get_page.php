<?php

/* START PAGE VARIABLES */

	$SITE_ROOT = '../../../';

/* END PAGE VARIABLES */



/* START EXTERNALS */

	require($SITE_ROOT . 'setup.php');

/* END EXTERNALS */



/************************************************************************************/
/*                               START PAGE                                         */
/************************************************************************************/
	
	/* START PAGE VARIABLES */
	
		// Title displayed in the browser.
		$page_title = 'Home';
		
		// Content for the template to render.
		$page_template = '1_column';
		$page_body = requestVar('page', 'home');
	
	/* END PAGE VARIABLES */
	
	
	
	switch($page_body)
	{
	
		default:
		case 'home':
		{
			$page_title = 'Home';
			
			$portfolioCategories = $DB->FetchAll('SELECT * FROM portfolio_categories WHERE 1');
			
			$featured_portfolio = $DB->FetchAll('SELECT * FROM portfolio WHERE featured = 1 ORDER BY display_order ASC');
			
			break;
		}
		
		case 'resume':
		{
			$page_title = 'Resume';
			
			$Breadcrumbs->Add('Resume', '/resume');
			
			$workHistory = $DB->FetchAll('SELECT * FROM work_history WHERE 1 ORDER BY end_date DESC');
			
			$education = $DB->FetchAll('SELECT * FROM education WHERE 1 ORDER BY id DESC');
			
			$skills = $DB->FetchAllAndGroup
			(
				'SELECT 
					skills.*,
					skill_categories.title AS category
				FROM 
					skills,
					skill_categories
				WHERE
					skills.category = skill_categories.id
				ORDER BY
					experience DESC',
				null,
				'category'
			);
			
			break;
		}
		
		case 'portfolio':
		{
			$page_title = 'Portfolio';
			
			$Breadcrumbs->Add('Portfolio', '/portfolio');
			
			switch($page_mode)
			{
				default:
				{
					$portfolioCategories = $DB->FetchAll('SELECT * FROM portfolio_categories WHERE 1');
					
					if(requestVar('pf', false) > 0)
					{
						$portfolio = $DB->FetchAll(
						'SELECT 
							portfolio.* 
						FROM 
							portfolio,
							portfolio_category_entries
						WHERE 
								portfolio.id = portfolio_category_entries.portfolio_id
							AND portfolio_category_entries.category_id = ?
						ORDER BY 
							portfolio.display_order ASC',
						array(requestVar('pf')));
					}
					else
					{
						$portfolio = $DB->FetchAll(
						'SELECT 
							* 
						FROM 
							portfolio
						WHERE 
							1
						ORDER BY 
							portfolio.display_order ASC');
					}
					
					break;
				}
				
				case 'page':
				{
					$page_body = 'portfolio_entry';
					
					$entry = $DB->Fetch('SELECT * FROM portfolio WHERE id = ?', array($page_action));
					
					$Breadcrumbs->Add($entry['title'], '/portfolio/page/' . $page_action);
					
					break;
				}
			}
			
			break;
		}
		
	}
	
/************************************************************************************/
/*                                END PAGE                                          */
/************************************************************************************/



require_once($DIRECTORY['templates'] . 'body/centerCol/body_centerColumn_upper.php');

require_once($DIRECTORY['pages'] . $page_body . '.php');

require_once($DIRECTORY['templates'] . 'body/centerCol/body_centerColumn_lower.php');

?>

<script>document.title = "Sibo - <?=$page_title?>";</script>