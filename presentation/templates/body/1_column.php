	<body>
	
	
	
		<!-- START PRELOADER -->

			<div id="preloader">
			
				<div class="socket">
				
					<div class="bubble-loader">
					
						<div class="1"></div>
						
						<div class="2"></div>
						
						<div class="3"></div>
						
						<div class="4"></div>
						
						<div class="5"></div>
						
					</div>
				  
				</div>
				
			</div>

		<!-- END PRELOADER -->
	  
	  
		
		<!-- START PAGE CONTENTS -->
			
			<div class="left-bio">
				
				<div class="name">
				
					<h1>
					
						<span class="first"><?=$_SETTINGS['personal']['first_name']?></span>
						<br>
						<span class="last"><?=$_SETTINGS['personal']['last_name']?></span>
					
					</h1>
					
				</div>
				
				<br>
			
				<img src="<?=$PRETTY_DIRECTORY['img']?>profile-image2.png" alt="profile image" style="border-radius:5px;width:100px;">
				
				<br>
				<br>
				
				<span class="title">Application and<br>Web Developer</span>
				
				<br>
				<br>
				
				<span class="email"><a href="mailto:<?=$_SETTINGS['personal']['email']?>"><?=$_SETTINGS['personal']['email']?></a></span>
				
				<br>
				
				<span class="phone"><?=$_SETTINGS['personal']['phone']?></span>
				
				<br>
				<br>
				
				<a href="https://www.linkedin.com/pub/christian-sibo/22/789/250" target="_blank">LinkedIn</a>
				
				<br>
				
				<a href="https://bitbucket.org/csibo/" target="_blank">BitBucket</a>
				
				<br>
				
				<a href="https://github.com/Siegen" targe="_blank">GitHub</a>
			
			</div>
			
			<div class="top-bio">
			
				<table style="width:95%;">
				
					<tr style="vertical-align:center;text-align:center;">
					
						<td>
			
							<img src="<?=$PRETTY_DIRECTORY['img']?>profile-image2.png" alt="profile image" style="border-radius:5px;width:100px;">
				
						</td>
						
						<td style="text-align:left;">
				
							<div class="name" style="margin:0;padding:0;">
							
								<h1 style="margin:0;padding:0;">
								
									<span class="first"><?=$_SETTINGS['personal']['first_name']?></span>
									
									<span class="last"><?=$_SETTINGS['personal']['last_name']?></span>
								
								</h1>
								
							</div>
							
							<br>
							<br>
							
							<span class="title" style="">Application and Web Developer</span>
				
						</td>
						
						<td>
				
							<span class="email"><a href="mailto:<?=$_SETTINGS['personal']['email']?>"><?=$_SETTINGS['personal']['email']?></a></span>
							
							<br>
							
							<span class="phone"><?=$_SETTINGS['personal']['phone']?></span>
							
						</td>
						
						<td style="text-align:right;">
							
							<a href="https://www.linkedin.com/pub/christian-sibo/22/789/250" target="_blank">LinkedIn</a>
							
							<br>
							
							<a href="https://bitbucket.org/csibo/" target="_blank">BitBucket</a>
							
							<br>
							
							<a href="https://github.com/Siegen" targe="_blank">GitHub</a>
						
						</td>
					
					</tr>
				
				</table>
			
			</div>
			
			
		
			<div class="content">
			
<?php 			require('header/body_header.php'); ?>
				
				<div class="page-data-target"></div>
				
			</div>
			
			
			
			<div class="right-bio"></div>
			
		<!-- END PAGE CONTENTS -->
		
		
		
	</body>