<!DOCTYPE html>
<html lang="en">

<?php

	// <head> ... </head>
	require('head/head.php');
	
	
	
	// Flush output buffers to let browser start 
	// requesting HEAD assets immediately.
	flush();
	ob_flush();
	
	
	
	// <body> ... </body>
	require('body/1_column.php');

?>

</html>