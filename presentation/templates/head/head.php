	<head>
	
		<!-- START METADATA -->
		
			<meta charset="utf-8">
			<meta http-equiv="content-type" content="text/html; charset=utf-8;" />
			<meta http-equiv="content-style-type" content="text/css" />
			<meta http-equiv="content-language" content="en" />
			<meta http-equiv="imagetoolbar" content="no" />
			
			<meta name="resource-type" content="document" />
			<meta name="distribution" content="global" />
			
			<meta name="contact" content="cpsibo@gmail.com">
			<meta name="keywords" content="Christian Sibo Developer Programmer Web Application C# C++ Perl PHP MySQL CSS HTML AJAX Comet Portfolio Resume" />
			<meta name="description" content="Portfolio and resume for Christian Sibo, Colorado-based web and application developer." />
			
			<link rel="shortcut icon" type="image/png" href="<?=$DIRECTORY['img']?>favicon.png" />
		
			<title>Sibo - <?=$page_title?></title>
			
		<!-- END METADATA -->
		
		
		
		<!-- START STYLESHEETS -->
		
<?php		foreach($styles as $index => $style): ?>

<?php			if(file_exists($DIRECTORY['css'] . $style . '.css')): ?>

					<link href="<?=($PRETTY_DIRECTORY['css'] . $style)?>.css" rel="stylesheet" />
					
<?php			else: ?>

					<link href="<?=$style?>" rel="stylesheet" />
					
<?php			endif ?>
			
<?php		endforeach ?>
		
		<!-- END STYLESHEETS -->
		
		
		
		<!-- START SCRIPTS -->
		
<?php		foreach($scripts as $index => $script): ?>

<?php			if(file_exists($DIRECTORY['js'] . $script . '.js')): ?>

					<script type='text/javascript' src='<?=($PRETTY_DIRECTORY['js'] . $script)?>.js'></script>
					
<?php			else: ?>

					<script type='text/javascript' src='<?=$script?>'></script>
					
<?php			endif ?>
			
<?php		endforeach ?>


		
			<!-- START ANALYTICS -->
		
				
			
			<!-- END ANALYTICS -->
			
			
			
			<script>
			
				// Retrieve global parameters.
				var $_POST = <?php echo json_encode($_POST); ?>;
				var $_GET = <?php echo json_encode($_GET); ?>;
				
			</script>
			
		<!-- END SCRIPTS -->
		
	</head>