<div class="desc" style="margin-bottom:0px;text-align:center;">
	
	<a href="documents/Resume - Christian Sibo.docx" target="_blank"><img src="<?=$DIRECTORY['img']?>social_icons/word.png" style="width:20px;top:5px;"> Word Version</a>
	
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	
	<a href="documents/Resume - Christian Sibo.pdf" target="_blank"><img src="<?=$DIRECTORY['img']?>social_icons/pdf.gif" style="width:20px;top:5px;"> PDF Version</a>
	
</div>
	
<!--
<div class="desc">
	<h4>Professional profile</h4>
	<p>Most of my experience has been with C#, PHP, and Perl so I am very familiar with the Visual Studios/.NET environments and LAMP stacks.</p>
</div>
-->



<br>
<hr>
<br>



<style>

	li
	{
		margin-bottom: 10px;
		text-indent: -20px;
		padding-left: 10px;
	}
	
	.right-list-entry
	{
		display:block;
		list-style:none;
		padding-right:30px;
	}
	
	.right-list-entry:before
	{
		display:none;
		content:"";
	}
	
	.right-list-entry:after
	{
		display:marker;
		content:"\020 \2770";
	}
	
</style>



<div>

	<div class="vertical-text-wrapper" style="position:absolute;height:460px;width:50px;">
	
		<h2 class="vertical-text uppercase" style="font-size:500%;opacity:0.2;color:rgb(0,0,0);">Experience</h2>
		
	</div>

	<?php	foreach($workHistory as $index => $work): ?>

		<?php
		
		$diff = date_diff(date_create($work['start_date']), date_create($work['end_date'] != '5000-00-00' ? $work['end_date'] : date('Y-m-d')));
		
		$matches = null;
		preg_match('|(.+)\.\d+$|', preg_replace('|\-|', '.', $work['start_date']), $matches);
		$start = $matches[1];
		
		if($work['end_date'] == '5000-00-00')
		{
			$end = 'Current';
		}
		else
		{
			$matches = null;
			preg_match('|(.+)\.\d+$|', preg_replace('|\-|', '.', $work['end_date']), $matches);
			$end = $matches[1];
		}
		
		?> 

		<table style="width:80%;margin:0 auto;<?=($index < count($workHistory) - 1)?'border-bottom:1px solid rgb(150,150,150);padding-bottom:30px;':''?>">
		
			<tr>
				
				<td colspan="99">
				
					<h3 class="title" style="margin:0px;padding:0px;left:-20px;font-size:175%;"><?=$work['position']?></h3>
					
				</td>
				
			</tr>
			
			<tr>
			
				<td colspan="99">
				
					<br>
				
					<div style="display:inline-block;margin-right:20px;">
					
						<img src="<?=$DIRECTORY['img'] . 'work_history/' . $work['logo']?>" style="height:50px;max-width:100px;margin:auto;border-radius:5px;opacity:0.8;top:5px;">
						
					</div>
				
					<div style="display:inline-block;">
						
						<span class="title"><b><?=$work['company_name']?></b></span>
						
						<br>
						
						<span class="time"><?=($start != '0000.00')?$start . ' - ':''?><?=$end?>&nbsp;&nbsp;&nbsp;(<?=$diff->y >= 1 ? $diff->y . ' year' . ($diff->y > 1 ? 's ' : ' ') : '' ?><?=$diff->m?> months)</span>
						
						<br>
						
						<span class="time"><?=$work['company_country']?></span>
					
					</div>
					
				</td>
				
			</tr>
			
			<tr>
			
				<td style="padding:20px;" colspan=99>
				
					<p style="text-indent:20px;">
					
						<?=implode('</p><p style="text-indent:20px;">', explode(PHP_EOL, $work['description']))?>
					
					</p>
				
				</td>
			
			</tr>
			
			<tr>
			
				<td style="width:50%;vertical-align:top;">
				
					<h4>DUTIES</h4>
					
					<ul>
					
	<?php			foreach(explode(';', $work['duties']) as $duty): ?>

						<li>
						
							<?=$duty?>
							
						</li>

	<?php			endforeach ?>
					
					</ul>
					
				</td>
			
				<td style="width:50%;text-align:right;vertical-align:top;">
				
					<h4>SKILLS</h4>
					
					<ul>
					
	<?php			foreach(explode(',', $work['skills']) as $skill): ?>

						<li class="right-list-entry">
						
							<?=$skill?>
							
						</li>

	<?php			endforeach ?>
					
					</ul>
					
				</td>
				
			</tr>
			
			<tr style="vertical-align:top;text-align:center;">
			
				<td>
				
					<div id="supervisor-info-header-<?=$index?>" style="width:100%;margin-bottom:20px;cursor:pointer;" onClick="toggleSupervisorInfo(<?=$index?>);">
					
						<h4><i class="fa fa-chevron-down"></i> Supervisor Info <i class="fa fa-chevron-down"></i></h4>
						
					</div>
				
					<div id="supervisor-info-<?=$index?>" style="width:100%;display:none;">

						<?=implode('<br>', array_filter(Array($work['supervisor_name'], $work['supervisor_position'], $work['supervisor_email'], $work['supervisor_phone'])))?>
					
					</div>
					
				</td>
			
				<td>
				
					<div id="contact-info-header-<?=$index?>" style="width:100%;margin-bottom:20px;cursor:pointer;" onClick="toggleContactInfo(<?=$index?>);">
					
						<h4><i class="fa fa-chevron-down"></i> Contact Info <i class="fa fa-chevron-down"></i></h4>
						
					</div>
				
					<div id="contact-info-<?=$index?>" style="width:100%;display:none;">

						<?=implode('<br>', array_filter(Array($work['company_address'], $work['company_city'] . ', ' . $work['company_state'] . ' ' . $work['company_zip'], $work['company_email'], $work['company_phone'])))?>
					
					</div>
					
				</td>
			
			</tr>
			
		</table>
		
		<?=($index < count($workHistory) - 1)?'<br><br>':''?>

	<?php	endforeach ?>

</div>



<br>
<hr>
<br>


<style>

	.ui-widget-content
	{
		background: unset;
		border: none;
	}
	
	#tabs .ui-widget-content
	{
		background-color: rgba(255,255,255,0.3);
		border-radius: 5px;
	}
	
	.ui-widget-header
	{
		background: unset;
		border: none;
	}
	
	.skill-cat-list li:before
	{
		display:none;
	}
	
	.skill-cat-list li
	{
		font-size: 120%;
		padding-left:20px !important;
		padding-right:20px !important;
	}
	
	.skill
	{
		font-weight:bold;
		text-size:110%;
	}
	
	.rating
	{
		position:absolute;
		right:0;
		transform:translate(0,-100%);
	}
	
	.skills .description
	{
		text-indent:20px;
		padding-left:10px;
		margin-bottom:30px;
	}
	
</style>



<div>

	<div class="vertical-text-wrapper" style="position:absolute;height:280px;width:50px;">
	
		<h2 class="vertical-text uppercase" style="font-size:500%;opacity:0.2;color:rgb(0,0,0);">Skills</h2>
		
	</div>

	<div id="tabs" style="width:90%;margin:0 auto;left:10px;">

		<ul class="skill-cat-list">
		
	<?php	foreach($skills as $index => $skillCat): ?>

				<li><a href="#<?=$index?>"><?=$index?></a></li>
		
	<?php	endforeach ?>
		
		</ul>

	<?php	foreach($skills as $index => $skillCat): ?>

		<div id="<?=$index?>">
			
			<ul class="skills">

	<?php		foreach($skillCat as $index2 => $skill): ?>

				<li>
				
					<span class="skill"><?=$skill['title']?></span>

<?php				if($skill['experience'] > 0):	?>
					
						<div class="rating">

<?php						for($i = $skill['experience']; $i < 5; $i++):	?>
							
								<i class="fa fa-circle-thin" style="color:rgba(50,50,50,0.5);"></i>&nbsp;
								
<?php						endfor	?>

<?php						for($i = 0; $i < $skill['experience']; $i++):	?>

								<i class="fa fa-circle" style="color:rgb(83, 178, 85);"></i>&nbsp;
								
<?php						endfor	?>

						</div>

<?php				endif	?>
					
					<br />
					
					<div class="description"><?=$skill['details']?></div>
					
				</li>
				
	<?php		endforeach ?>

			</ul>

		</div>
		
	<?php	endforeach ?>
		
	</div>

</div>



<br>
<hr>
<br>



<div>

	<h2 class="vertical-text uppercase" style="font-size:500%;opacity:0.15;color:rgb(0,0,0);">Education</h2>

	<?php	foreach($education as $index => $school): ?>

		<?php
		
		$matches = null;
		preg_match('|(.+)\.\d+\.\d+$|', preg_replace('|\-|', '.', $school['start']), $matches);
		$start = $matches[1];
		
		$matches = null;
		preg_match('|(.+)\.\d+\.\d+$|', preg_replace('|\-|', '.', $school['end']), $matches);
		$end = $matches[1];
		
		?> 

		<table style="width:80%;margin:0 auto;<?=($index < count($education) - 1)?'border-bottom:1px solid rgb(150,150,150);padding-bottom:30px;':''?>">
		
			<tr>
				
				<td>
				
					<h3 class="title" style="margin:0px;padding:0px;left:-20px;font-size:175%;"><?=$school['degree']?></h3>
					
					<br>
					
					<span class="title"><?=$school['school']?></span>
					
					<br>
					
					<span class="time"><?=($start != '0000')?$start . ' - ':''?><?=$end?></span>
					
					<br>
					
					<span class="time"><?=$school['city']?>, <?=$school['state']?>, <?=$school['country']?></span>
					
				</td>
			
				<td class="logo" style="float:right;">
				
					<img src="<?=$DIRECTORY['img'] . 'education/' . $school['logo']?>" style="max-height:100px;max-width:100px;margin:auto;border-radius:5px;opacity:0.75;">
					
				</td>
				
			</tr>
			
	<?php	if($school['awards'] != null && count(explode(',', $school['awards'])) > 0): ?>
			
			<tr>
			
				<td>
					
					<h4>Awards</h4>
					
					<ul>
					
	<?php			foreach(explode(',', $school['awards']) as $award): ?>

						<li>
							<?=$award?>
						</li>

	<?php			endforeach ?>
					
					</ul>
					
				</td>
			
			</tr>
			
	<?php	endif ?>
			
		</table>
		
		<?=($index < count($education) - 1)?'<br><br>':''?>

	<?php	endforeach ?>

</div>



<script>

	$(".vertical-text-wrapper").stick_in_parent({offset_top:20})
		.on("sticky_kit:unstick", function(e)
		{
			$(e.target).css('position','absolute');
		});
		
	function toggleSupervisorInfo(id)
	{
		if($('#supervisor-info-' + id).css('display') == 'none')
		{
			$('#supervisor-info-' + id).velocity("slideDown", { duration: 500 });
		}
		else
		{
			$('#supervisor-info-' + id).velocity("slideUp", { duration: 500 });
		}
		
		$('#supervisor-info-header-' + id).find('.fa').toggleClass('fa-chevron-down fa-chevron-up');
	}
		
	function toggleContactInfo(id)
	{
		if($('#contact-info-' + id).css('display') == 'none')
		{
			$('#contact-info-' + id).velocity("slideDown", { duration: 500 });
		}
		else
		{
			$('#contact-info-' + id).velocity("slideUp", { duration: 500 });
		}
		
		$('#contact-info-header-' + id).find('.fa').toggleClass('fa-chevron-down fa-chevron-up');
	}

</script>