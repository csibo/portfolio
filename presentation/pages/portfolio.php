
<div class="portfolio-filter">

	Filter:

	<select class="select2 ajax-widget-select" name="pf" data-page='portfolio'>
	
		<option value="0">All</option>

		<?php	foreach($portfolioCategories as $index => $category): ?>

			<option value="<?=$category['id']?>"<?=requestVar('pf') == $category['id']? ' selected' : ''?>><?=$category['title']?></option>

		<?php	endforeach ?>

	</select>

</div>



<br>
<hr>
<br>

<?=printPortfolioEntries2($portfolio)?>


<script>

</script>