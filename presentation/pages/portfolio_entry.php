
<?=printPortfolioEntries2(array($entry))?>

<?php if($entry['slider_images'] != null): ?>
	
	<!-- Jssor Slider Begin -->
		<!-- You can move inline styles to css file or css block. -->
		<div id="slider1_container" style="position: relative; width: 1200px; height: 675px; overflow: hidden; top: 3px; border-radius: 5px;">

			<!-- Loading Screen -->
			<div u="loading" style="position: absolute; top: 0px; left: 0px;">
				<div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
					background-color: #000; top: 0px; left: 0px;width: 100%;height:100%;">
				</div>
				<div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
					top: 0px; left: 0px;width: 100%;height:100%;">
				</div>
			</div>

			<!-- Slides Container -->
			<div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 1200px; height: 675px;overflow: hidden;">
				
	<?php		foreach(json_decode($entry['slider_images']) as $index => $item): ?>
				
	<?php			if($item->type == 0): ?>
				
						<div>
							<img u="image" src="<?=$PRETTY_DIRECTORY['img'] . 'portfolio/' . $item->image?>" />
							<img u="thumb" src="<?=$PRETTY_DIRECTORY['img'] . 'portfolio/' . $item->image?>" />
						</div>
					
	<?php			else: ?>

						<div u="player" style="position: relative; top: 0px; left: 0px; width: 1200px; height: 675px; overflow: hidden;">
						
							<!-- iframe in ebay page is not allowed, youtube iframe video is not supported for ebay listing -->
							<iframe pHandler="ytiframe" pHideControls="0" width="1200" height="675" style="z-index: 0;" url="<?=$PRETTY_DIRECTORY['img'] . 'portfolio/' . $item->image?>" frameborder="0" scrolling="no"></iframe>
							
						</div>
					
	<?php			endif ?>	

	<?php		endforeach ?>
				
			</div>
			
			<!-- Thumbnail Navigator Skin Begin -->
			<div u="thumbnavigator" class="jssort07" style="position: absolute; width: 1200px; height: 100px; left: 0px; bottom: 0px; overflow: hidden; ">
				<div style=" background-color: #000; filter:alpha(opacity=30); opacity:.3; width: 100%; height:100%;"></div>
				<div u="slides" style="cursor: move;">
					<div u="prototype" class="p" style="POSITION: absolute; WIDTH: 99px; HEIGHT: 66px; TOP: 0; LEFT: 0;">
						<div u="thumbnailtemplate" class="i" style="position:absolute;"></div>
						<div class="o">
						</div>
					</div>
				</div>
				<!-- Thumbnail Item Skin End -->
				
				<!-- Arrow Left -->
				<span u="arrowleft" class="jssora11l" style="width: 37px; height: 37px; top: 123px; left: 8px;">
				</span>
				<!-- Arrow Right -->
				<span u="arrowright" class="jssora11r" style="width: 37px; height: 37px; top: 123px; right: 8px">
				</span>
				<!-- Arrow Navigator Skin End -->
			</div>
			<!-- ThumbnailNavigator Skin End -->
			<a style="display: none" href="http://www.jssor.com">Image Slider</a>
			<!-- Trigger -->
		</div>
	<!-- Jssor Slider End -->
	
	
	
	<script>
	
		applySlider();
		
	</script>

<?php 	endif ?>


<br>


<?php 	if(!empty($entry['website'])): ?>

<a href="<?=$entry['website']?>" target="_blank">

	<div class="button">
	
		<div class="text">
		
			Website
			
		</div>
		
	</div>
	
</a>

<?php 	endif ?>


<?php 	if(!empty($entry['repository_link'])): ?>

<a href="<?=$entry['repository_link']?>" target="_blank">

	<div class="button">
	
		<div class="text">
		
			Repository
			
		</div>
		
	</div>
	
</a>

<?php 	endif ?>


<br>
<br>



<div>

	<?=$entry['description']?>
	
</div>

<script>

	$('#slider1_container').velocity("fadeIn", 1000);

	$('p').velocity("transition.fadeIn", 
	{
		duration: 1000,
		stagger: 250
	});
	
	$('li').velocity("transition.fadeIn", 
	{
		duration: 200,
		stagger: 50
	});
	
</script>