

<div class="info">

	<table style="width:100%;">
	
		<tr>
		
			<td class="desc" style="width:60%;">
			
				<p>Welcome to my personal site.</p>
				
				<p>I'm an application and web developer, specializing in C# and PHP. I also specialize in tool development and scripting support in Perl and Bash.</p>
				
				<p>My full resume and portfolio are accessible via the links to the right. Entries in the portfolio are links to more information, including access to repositories or demos. If you have any questions or comments, please feel free to contact me via the information to the left.</p>
				
			</td>
			
			
			
			<td class="links-wrapper" style="width:35%;font-size:200%;text-align:right;">
				
				<a href="/resume" data-page="resume" class="ajax">Resume <i class="fa fa-graduation-cap"></i></a>
				
				<br>
				<br>
				
				<a href="/portfolio" data-page="portfolio" class="ajax">Portfolio <i class="fa fa-briefcase"></i></a>
				
			</td>
		
		</tr>
	
	</table>

</div>
	
	
	
<style>

	.links-wrapper a:hover
	{
		color:rgb(115, 140, 165);
	}

</style>



<br>
<hr>
<br>

<h1 style="text-align:center;">- Featured Projects -</h1>

<br>

<?=printPortfolioEntries2($featured_portfolio)?>